/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class connect {
    public class DatabaseConnection {
    private static final String URL = "jdbc:mysql://localhost:3306/pengelolaan_restoran"; // URL database
    private static final String USER = "root"; // Username database
    private static final String PASSWORD = ""; // Password database (kosong jika tidak ada)

    private static Connection connection;

    // Metode untuk mendapatkan koneksi ke database
    public static Connection getConnection() {
        if (connection == null) {
            try {
                // Muat driver MySQL JDBC
                Class.forName("com.mysql.cj.jdbc.Driver");
                // Buat koneksi ke database
                connection = DriverManager.getConnection(URL, USER, PASSWORD);
                System.out.println("Koneksi ke database berhasil!");
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
                System.out.println("Koneksi ke database gagal: " + e.getMessage());
            }
        }
        return connection;
    }

    // Metode untuk menutup koneksi ke database
    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                System.out.println("Koneksi ke database ditutup.");
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Gagal menutup koneksi ke database: " + e.getMessage());
            }
        }
    }
}
}
